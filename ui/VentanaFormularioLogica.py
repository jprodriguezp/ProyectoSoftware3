from PyQt5.QtWidgets import QMainWindow
from PyQt5 import QtCore, QtWidgets
from pandas import DataFrame


from ui.VentanaFormulario import Ui_MainWindowFormulario

"""Ventana donde se presenta un formulario.
En esta ventana se muestra el formulario correspodiente a una 
respuesta hecha en la encuesta elaborada en Google Forms

"""


class VentanaFormularioLogica(QMainWindow):

    numero_encuesta: int
    conjunto_datos: DataFrame
    respuestas:()
    izquierda = False
    derecha = False
    arriba = False
    centro = False
    abajo = False
    def __init__(self, numero: int, datos: DataFrame, respuestas, parent=None):
        QMainWindow.__init__(self, parent)

        self.numero_encuesta = numero
        self.conjunto_datos = datos
        self.respuestas = respuestas

        self.ui = Ui_MainWindowFormulario()
        self.ui.setupUi(self)
        self.armar_formulario(self.numero_encuesta, self.conjunto_datos)



    """Funcion que arma el formulario.
    Se arma un formulario de acuerdo al numero de la respuesta de 
    la encuesta hecha en Google Forms
    
    """

    def armar_formulario(self, numero_encuesta: int, conjunto_datos: DataFrame):

        if numero_encuesta == -1:
            self.crear_form_predecido()
        else:


            estilo = ("#frameComprador{\n"
                      "border: 0.5px solid gray;\n"
                     # "border-radius: 15px;\n"
                      "background:white;\n"                     
                      "}\n"
                      "#frameProducto{\n"
                      "border: 0.5px solid gray;\n"
                      #"border-radius: 15px;\n"
                      "background:white;\n"    
                      "}\n"
                      "#frameEnvio{\n"
                      "border: 0.5px solid gray;\n"
                      #"border-radius: 15px;\n"
                      "background:white;\n"    
                      "}\n"
                      "")

            # Orden en el que deben aparecer los datos del comprador
            columna3 = str(conjunto_datos.iloc[numero_encuesta][3])

            # Orden en el que deben aparecer los datos del envio
            columna4 = str(conjunto_datos.iloc[numero_encuesta][4])

            # Orden en el que deben aparecer los datos del producto
            columna5 = str(conjunto_datos.iloc[numero_encuesta][5])

            # ¿Cual de las siguientes partes deberia encabezar la seccion de envio?
            columna6 = str(conjunto_datos.iloc[numero_encuesta][6])

            # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del comprador?
            columna8 = str(conjunto_datos.iloc[numero_encuesta][8])

            # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del producto?
            columna9 = str(conjunto_datos.iloc[numero_encuesta][9])

            # Posicion del contenedor de los datos del comprador
            columna14 = str(conjunto_datos.iloc[numero_encuesta][14])

            # Posicion del contenedor de los datos del envio
            columna15 = str(conjunto_datos.iloc[numero_encuesta][15])

            # Posicion del contenedor de los datos del producto
            columna16 = str(conjunto_datos.iloc[numero_encuesta][16])




            if columna14 == 'Izquierda':
                self.ui.frameComprador.setGeometry(QtCore.QRect(0, 0, 321, 341))
                self.ui.frame.setGeometry(QtCore.QRect(0, 0, 321, 41))
                self.ui.line.setGeometry(QtCore.QRect(20, 300, 301, 20))
                self.ui.pushButton.setGeometry(QtCore.QRect(20, 310, 81, 23))
                self.ui.line_4.setGeometry(QtCore.QRect(0, 30, 351, 16))
                self.ui.MainWindowFormulario.setGeometry(QtCore.QRect(20, 0, 1041, 1000))
            elif columna14 == 'Derecha':
                self.ui.frameComprador.setGeometry(QtCore.QRect(675, 0, 325, 230))
            elif columna14 == 'Arriba':
                self.ui.frameComprador.setGeometry(QtCore.QRect(335, 0, 325, 230))
            elif columna14 == 'Centro':
                self.ui.frameComprador.setGeometry(QtCore.QRect(335, 200, 325, 230))
            elif columna14 == 'Abajo':
                self.ui.frameComprador.setGeometry(QtCore.QRect(335, 350, 325, 230))

            if columna15 == 'Izquierda':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(0, 0, 321, 341))
                self.ui.frame_2.setGeometry(QtCore.QRect(0, 0, 321, 41))
                self.ui.line_3.setGeometry(QtCore.QRect(20, 300, 301, 20))
                self.ui.pushButton_3.setGeometry(QtCore.QRect(20, 310, 81, 23))
                self.ui.line_6.setGeometry(QtCore.QRect(0, 30, 351, 16))
            elif columna15 == 'Derecha':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(675, 0, 325, 300))
            elif columna15 == 'Arriba':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 0, 325, 230))
            elif columna15 == 'Centro':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 200, 325, 230))
            elif columna15 == 'Abajo':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 300, 325, 230))
                self.ui.frame_2.setGeometry(QtCore.QRect(0, 0, 325, 41))

            if columna16 == 'Izquierda':
                self.ui.frameProducto.setGeometry(QtCore.QRect(0, 0, 321, 341))
                self.ui.frame_4.setGeometry(QtCore.QRect())
                self.ui.frame_3.setGeometry(QtCore.QRect(0, 0, 321, 41))
                self.ui.line_2.setGeometry(QtCore.QRect(20, 300, 301, 20))
                self.ui.pushButton_2.setGeometry(QtCore.QRect(20, 310, 81, 23))
                self.ui.line_5.setGeometry(QtCore.QRect(0, 30, 351, 16))
            elif columna16 == 'Derecha':
                self.ui.frameProducto.setGeometry(QtCore.QRect(675, 0, 351, 341))
            elif columna16 == 'Arriba':
                self.ui.frameProducto.setGeometry(QtCore.QRect(335, 0, 351, 341))
            elif columna16 == 'Centro':
                self.ui.frameProducto.setGeometry(QtCore.QRect(335, 200, 351, 341))
            elif columna16 == 'Abajo':
                self.ui.frameProducto.setGeometry(QtCore.QRect(335, 350, 351, 341))

            if columna3 == 'Teléfono,  Apellidos, Nombres, Documento':
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)
            elif columna3 == 'Nombres, Apellidos, Documento, Teléfono':
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
            elif columna3 == 'Documento, Nombres, Apellidos, Teléfono':
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
            elif columna3 == 'Nombres, Apellidos, Teléfono, Documento':
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
                self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
                self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
                self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
                self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)

            if columna4 == 'Ciudad, Departamento, Domicilio, Teléfono, Nombre de quien recibe':
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblDepartamento)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtDepartamento)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
            elif columna4 == 'Nombre de quien recibe, Domicilio, Ciudad, Departamento, Teléfono':
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblDepartamento)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtDepartamento)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
            elif columna4 == 'Departamento, Ciudad, Domicilio, Nombre de quien recibe, Teléfono':
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
            elif columna4 == 'Nombre de quien recibe, Teléfono, Departamento, Ciudad, Domicilio':
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
                self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
                self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDepartamento)
                self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDepartamento)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
                self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
                self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)

            if columna5 == 'Precio - Nombre - Costo envió - Imagen - Cantidad Disponible':
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
                ##self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
            elif columna5 == 'Nombre - Precio - Imagen - Costo envió - Cantidad Disponible':
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
                ##self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
            elif columna5 == 'Imagen - Nombre - Precio - Cantidad Disponible - Costo envió':
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
                ##self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
            elif columna5 == 'Nombre - Imagen - Precio - Cantidad Disponible - Costo envió':
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
                ##self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
            elif columna5 == 'Cantidad Disponible - Nombre - Imagen - Precio - Costo envió':
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
                self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
                self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
                self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
                ## self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
                self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
                self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)

            # ¿Cual de las siguientes partes deberia encabezar la seccion de envio?
            if columna6 == 'Forma de entrega':
                estilo += ("#lblDepartamento{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtTelefono_2:{\n"
                           "background-color: lightblue;\n"
                           "}\n")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna6 == 'Dirección del domicilio':
                estilo += ("#lblDomicilio{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtDomicilio:{\n"
                           "background-color: blue;\n"
                           "border:2px solid#aaa;\n"
                           "}\n")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna6 == 'Fecha de entrega':
                estilo += ("#lblCiudad{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtTelefono_2:{\n"
                           "background-color: lightblue;\n"
                           "}\n")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna6 == 'Telefono':
                estilo += ("#lblTelefonoEnvio{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtTelefono_2:{\n"
                           "background-color: lightblue;\n"
                           "}\n")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna6 == 'Nombre de quien recibe':
                estilo += ("#lblRecibe{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtRecibe:{\n"
                           "background-color: lightblue;\n"
                           "}\n")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)

            # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del comprador?
            if columna8 == 'Nombre':
                estilo += ("#lblNombresComprador{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtNombresComprador:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}\n")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna8 == 'Apellidos':
                estilo += ("#lblApellidos{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtApellidos:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna8 == 'Documento':
                estilo += ("#lblDocumento{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtDocumento:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna8 == 'Teléfono':
                estilo += ("#lblTelefonoComprador{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtTelefonoComprador:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)

            # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del producto?
            if columna9 == 'Nombre del producto':
                estilo += ("#lblNombreProducto{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtRecibe:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna9 == 'Precio del producto':
                estilo += ("#lblPrecio{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtRecibe:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna9 == 'Cantidad disponible':
                estilo += ("#lblCantDisponible{\n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtRecibe:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")
                self.ui.MainWindowFormulario.setStyleSheet(estilo)
            elif columna9 == 'Imagen del producto':
                estilo += ("#lblImagen{ \n"
                           "text-decoration: underline;\n"
                           "text-transform: uppercase;\n"
                           "color : blue;\n"
                           "font-style: oblique;\n"
                           "}\n"
                           "#txtRecibe:{\n"
                           "background-color: rgb(85, 85, 255);\n"
                           "}")

                self.ui.MainWindowFormulario.setStyleSheet(estilo)


    def crear_form_predecido(self):

        self.izquierda = False
        self.derecha = False
        self.arriba = False
        self.centro = False
        self.abajo = False

        estilo = ("#frameComprador{\n"
                  "border: 0.5px solid gray;\n"
                  #"border-radius: 15px;\n"
                  "background:white;\n"
                  "}\n"
                  "#frameProducto{\n"
                  "border: 0.5px solid gray;\n"
                  #"border-radius: 15px;\n"
                  "background:white;\n"
                  "}\n"
                  "#frameEnvio{\n"
                  "border: 0.5px solid gray;\n"
                  #"border-radius: 15px;\n"
                  "background:white;\n"
                  "}\n"
                  "")

        # Orden en el que deben aparecer los datos del comprador
        columna3 = str(self.respuestas[1])

        # Orden en el que deben aparecer los datos del envio
        columna4 = str(self.respuestas[10])

        # Orden en el que deben aparecer los datos del producto
        columna5 = str(self.respuestas[7])

        # ¿Cual de las siguientes partes deberia encabezar la seccion de envio?
        columna6 = str(self.respuestas[12])

        # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del comprador?
        columna8 = str(self.respuestas[3])

        # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del producto?
        columna9 = str(self.respuestas[7])

        # Posicion del contenedor de los datos del comprador
        columna14 = str(self.respuestas[0])
        if columna14 == 'Izquierda':
            self.ui.frameComprador.setGeometry(QtCore.QRect(0, 0, 325, 200))
            self.izquierda = True
        elif columna14 == 'Derecha':
            self.ui.frameComprador.setGeometry(QtCore.QRect(675, 0, 325, 200))
            self.derecha = True
        elif columna14 == 'Arriba':
            self.ui.frameComprador.setGeometry(QtCore.QRect(335, 0, 325, 200))
            self.arriba = True
        elif columna14 == 'Centro':
            self.ui.frameComprador.setGeometry(QtCore.QRect(335, 200, 325, 200))
            self.centro = True
        elif columna14 == 'Abajo':
            self.ui.frameComprador.setGeometry(QtCore.QRect(335, 400, 325, 200))
            self.abajo = True

        # Posicion del contenedor de los datos del envio
        columna15 = str(self.respuestas[9])
        if columna15 == 'Izquierda' and self.izquierda == False:
            self.ui.frameEnvio.setGeometry(QtCore.QRect(0, 0, 325, 200))
            self.izquierda = True
        elif columna15 == 'Derecha' and self.derecha == False:
            self.ui.frameEnvio.setGeometry(QtCore.QRect(675, 0, 325, 200))
            self.derecha = True
        elif columna15 == 'Arriba' and self.arriba == False:
            self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 0, 325, 200))
            self.arriba = True
        elif columna15 == 'Centro' and self.centro == False:
            self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 200, 325, 200))
            self.centro = True
        elif columna15 == 'Abajo' and self.abajo == False:
            self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 400, 325, 200))
            self.abajo == True
        # Si la posicion esta ocupada se halla alguna posicion disponible
        else:
            columna15 = self.hallar_posicion_disponible()
            if columna15 == 'Izquierda':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(0, 0, 325, 200))
                self.izquierda = True
            elif columna15 == 'Derecha':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(675, 0, 325, 200))
                self.derecha = True
            elif columna15 == 'Arriba':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 0, 325, 200))
                self.arriba = True
            elif columna15 == 'Centro':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 200, 325, 200))
                self.centro = True
            elif columna15 == 'Abajo':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 400, 325, 200))
                self.abajo = True

        # Posicion del contenedor de los datos del producto
        columna16 = str(self.respuestas[5])

        if columna16 == 'Izquierda' and self.izquierda == False:
            self.ui.frameProducto.setGeometry(QtCore.QRect(0, 0, 351, 341))
            self.ui.line_2.setGeometry(QtCore.QRect(20, 300, 321, 50))
        elif columna16 == 'Derecha' and self.derecha == False:
            self.ui.frameProducto.setGeometry(QtCore.QRect(675, 0, 351, 341))
        elif columna16 == 'Arriba' and self.arriba == False:
            self.ui.frameProducto.setGeometry(QtCore.QRect(335, 0, 351, 341))
        elif columna16 == 'Centro' and self.centro == False:
            self.ui.frameProducto.setGeometry(QtCore.QRect(335, 200, 351, 341))
        elif columna16 == 'Abajo' and self.abajo == False:
            self.ui.frameProducto.setGeometry(QtCore.QRect(335, 400, 351, 341))
        # Si la posicion esta ocupada se halla alguna posicion disponible
        else:
            columna16 = self.hallar_posicion_disponible()
            if columna16 == 'Izquierda':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(0, 0, 325, 200))
                self.izquierda = True
            elif columna16 == 'Derecha':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(675, 0, 325, 200))
                self.derecha = True
            elif columna16 == 'Arriba':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 0, 325, 200))
                self.arriba = True
            elif columna16 == 'Centro':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 200, 325, 200))
                self.centro = True
            elif columna16 == 'Abajo':
                self.ui.frameEnvio.setGeometry(QtCore.QRect(335, 400, 325, 200))
                self.abajo = True


        if columna3 == 'Teléfono,  Apellidos, Nombres, Documento':
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)
        elif columna3 == 'Nombres, Apellidos, Documento, Teléfono':
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
        elif columna3 == 'Documento, Nombres, Apellidos, Teléfono':
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
        elif columna3 == 'Nombres, Apellidos, Teléfono, Documento':
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombresComprador)
            self.ui.formComprador.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombresComprador)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblApellidos)
            self.ui.formComprador.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtApellidos)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoComprador)
            self.ui.formComprador.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefonoComprador)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblDocumento)
            self.ui.formComprador.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtDocumento)

        if columna4 == 'Ciudad, Departamento, Domicilio, Teléfono, Nombre de quien recibe':
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblDepartamento)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtDepartamento)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
        elif columna4 == 'Nombre de quien recibe, Domicilio, Ciudad, Departamento, Teléfono':
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblDepartamento)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtDepartamento)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
        elif columna4 == 'Departamento, Ciudad, Domicilio, Nombre de quien recibe, Teléfono':
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
        elif columna4 == 'Nombre de quien recibe, Teléfono, Departamento, Ciudad, Domicilio':
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblRecibe)
            self.ui.formEnvio.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtRecibe)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblTelefonoEnvio)
            self.ui.formEnvio.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtTelefono_2)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblDepartamento)
            self.ui.formEnvio.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtDepartamento)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCiudad)
            self.ui.formEnvio.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCiudad)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblDomicilio)
            self.ui.formEnvio.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtDomicilio)

        if columna5 == 'Precio - Nombre - Costo envió - Imagen - Cantidad Disponible':
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
            ##self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
        elif columna5 == 'Nombre - Precio - Imagen - Costo envió - Cantidad Disponible':
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
            ##self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
        elif columna5 == 'Imagen - Nombre - Precio - Cantidad Disponible - Costo envió':
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
            ##self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
        elif columna5 == 'Nombre - Imagen - Precio - Cantidad Disponible - Costo envió':
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
            ##self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)
        elif columna5 == 'Cantidad Disponible - Nombre - Imagen - Precio - Costo envió':
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.ui.lblCantDisponible)
            self.ui.formProductos.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.ui.txtCantDisponible)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.ui.lblNombreProducto)
            self.ui.formProductos.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.ui.txtNombreProducto)
            self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.ui.lblImagen)
           ##self.ui.formProductos.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ui.txtImagen)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.ui.lblPrecio)
            self.ui.formProductos.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.ui.txtPrecio)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.ui.lblCostoEnvio)
            self.ui.formProductos.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.ui.txtCostoEnvio)

        # ¿Cual de las siguientes partes deberia encabezar la seccion de envio?
        if columna6 == 'Forma de entrega':
            estilo += ("#lblDepartamento{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtTelefono_2:{\n"
                       "background-color: lightblue;\n"
                       "}\n")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna6 == 'Dirección del domicilio':
            estilo += ("#lblDomicilio{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtDomicilio:{\n"
                       "background-color: blue;\n"
                       "border:2px solid#aaa;\n"
                       "}\n")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna6 == 'Fecha de entrega':
            estilo += ("#lblCiudad{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtTelefono_2:{\n"
                       "background-color: lightblue;\n"
                       "}\n")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna6 == 'Telefono':
            estilo += ("#lblTelefonoEnvio{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtTelefono_2:{\n"
                       "background-color: lightblue;\n"
                       "}\n")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna6 == 'Nombre de quien recibe':
            estilo += ("#lblRecibe{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}\n")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)

        # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del comprador?
        if columna8 == 'Nombre':
            estilo += ("#lblNombresComprador{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}\n")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna8 == 'Apellidos':
            estilo += ("#lblApellidos{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna8 == 'Documento':
            estilo += ("#lblDocumento{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna8 == 'Teléfono':
            estilo += ("#lblTelefonoComprador{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)

        # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del producto?
        if columna9 == 'Nombre del producto':
            estilo += ("#lblNombreProducto{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna9 == 'Precio del producto':
            estilo += ("#lblPrecio{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna9 == 'Cantidad disponible':
            estilo += ("#lblCantDisponible{\n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)
        elif columna9 == 'Imagen del producto':
            estilo += ("#lblImagen{ \n"
                       "text-decoration: underline;\n"
                       "text-transform: uppercase;\n"
                       "color : blue;\n"
                       "font-style: oblique;\n"
                       "}\n"
                       "#txtRecibe:{\n"
                       "background-color: lightblue;\n"
                       "}")
            self.ui.MainWindowFormulario.setStyleSheet(estilo)


    """ Metodo que halla una posicion disponible para agregar un contenedor
    Retorna una posicion disponible del formato
    
    """
    def hallar_posicion_disponible(self):
        if self.izquierda == False:
            return 'Izquierda'
        elif self.derecha == False:
            return 'Derecha'
        elif self.arriba == False:
            return 'Arriba'
        elif self.centro == False:
            return 'Centro'
        elif self.abajo == False:
            return 'Abajo'
