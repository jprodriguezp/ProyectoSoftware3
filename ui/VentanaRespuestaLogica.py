from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem
from pandas import DataFrame
from ui.VentanaPrincipal import Ui_MainWindow
from ui.VentanaRespuestas import Ui_MainWindow


""" Ventana donde se presentan las respuestas de la encuesta.
En esta clase se muestran mediante una tabla las respuestas
que se han hecho en la encuesta 

"""
class VentanaRespuestaLogica(QMainWindow):
    def __init__(self, datos : DataFrame, parent=None):
        QMainWindow.__init__(self, parent)
        self.datos = datos
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.iniciar_tabla(self.datos)
        self.crear_matriz(self.datos)

        self.ui.btnVolver.clicked.connect(self.action_button_volver)

    """ Funcion que carga los datos de las respuestas de la encuesta a TableWidget
     para ser visualizado por el usuario
     
    """
    def iniciar_tabla(self, datos: DataFrame):
        # Se modifican la cantidad de columnas que tendra la tabla
        self.ui.tableRespuestas.setColumnCount(20)
        # Se añaden los encabezados a la tabla
        self.ui.tableRespuestas.setHorizontalHeaderLabels(
            ['Marca Temporal'
                , 'Sexo'
                , '¿Compra regularmente por Internet?'
                , '¿Orden correcto de los datos del comprador?'
                , '¿Orden correcto de los datos de envio?'
                , '¿Orden corecto de los datos del producto?'
                , '¿Cual de las siguientes partes deberia encabezar la seccion de envio?'
                , '¿Cual de los siguientes datos considera el mas importante en el formato de compra?'
                ,
             '¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del comprador?'
                , '¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del producto?'
                , '¿Cuales datos del comprador consideran que son obligatorios?'
                , '¿Cuales datos del envio considera que son obligatorios?'
                , '¿Cuales datos del producto considera que son obligatorios?'
                , '¿Cuantos años tienes?'
                , '¿Donde cree que deberia ir la informacion del comprador?'
                , '¿Donde cree que deberia ir la informacion del envio?'
                , '¿Dónde cree que debería ir la información del producto?'
                , '¿Es usted una persona asalariada?'
                , '¿Nombre y apellido deberian estan juntos o por separado?'
                , '¿Son claras las preguntas?'

             ])
        row = 0
        # Recorrido de la cantidad de fila que tenga el conjunto de datos
        for fila in datos.iterrows():
            self.ui.tableRespuestas.insertRow(row)
            # Se almacenan los valores que tendran cada columna de la fila en la que se este iterando
            columna0 = QTableWidgetItem(str(datos.iloc[row][0]))
            columna1 = QTableWidgetItem(str(datos.iloc[row][1]))
            columna2 = QTableWidgetItem(str(datos.iloc[row][2]))
            columna3 = QTableWidgetItem(str(datos.iloc[row][3]))
            columna4 = QTableWidgetItem(str(datos.iloc[row][4]))
            columna5 = QTableWidgetItem(str(datos.iloc[row][5]))
            columna6 = QTableWidgetItem(str(datos.iloc[row][6]))
            columna7 = QTableWidgetItem(str(datos.iloc[row][7]))
            columna8 = QTableWidgetItem(str(datos.iloc[row][8]))
            columna9 = QTableWidgetItem(str(datos.iloc[row][9]))
            columna10 = QTableWidgetItem(str(datos.iloc[row][10]))
            columna11 = QTableWidgetItem(str(datos.iloc[row][11]))
            columna12 = QTableWidgetItem(str(datos.iloc[row][12]))
            columna13 = QTableWidgetItem(str(datos.iloc[row][13]))
            columna14 = QTableWidgetItem(str(datos.iloc[row][14]))
            columna15 = QTableWidgetItem(str(datos.iloc[row][15]))
            columna16 = QTableWidgetItem(str(datos.iloc[row][16]))
            columna17 = QTableWidgetItem(str(datos.iloc[row][17]))
            columna18 = QTableWidgetItem(str(datos.iloc[row][18]))
            columna19 = QTableWidgetItem(str(datos.iloc[row][19]))

            # Se asignan los valores al tableWidget para mostrar al usuario
            self.ui.tableRespuestas.setItem(row, 0, columna0)
            self.ui.tableRespuestas.setItem(row, 1, columna1)
            self.ui.tableRespuestas.setItem(row, 2, columna2)
            self.ui.tableRespuestas.setItem(row, 3, columna3)
            self.ui.tableRespuestas.setItem(row, 4, columna4)
            self.ui.tableRespuestas.setItem(row, 5, columna5)
            self.ui.tableRespuestas.setItem(row, 6, columna6)
            self.ui.tableRespuestas.setItem(row, 7, columna7)
            self.ui.tableRespuestas.setItem(row, 8, columna8)
            self.ui.tableRespuestas.setItem(row, 9, columna9)
            self.ui.tableRespuestas.setItem(row, 10, columna10)
            self.ui.tableRespuestas.setItem(row, 11, columna11)
            self.ui.tableRespuestas.setItem(row, 12, columna12)
            self.ui.tableRespuestas.setItem(row, 13, columna13)
            self.ui.tableRespuestas.setItem(row, 14, columna14)
            self.ui.tableRespuestas.setItem(row, 15, columna15)
            self.ui.tableRespuestas.setItem(row, 16, columna16)
            self.ui.tableRespuestas.setItem(row, 17, columna17)
            self.ui.tableRespuestas.setItem(row, 18, columna18)
            self.ui.tableRespuestas.setItem(row, 19, columna19)
            # Se incrementa el valor de la fila
            row = row + 1

    """Funcion que permite la creacion de la matriz numerica.
    El proposito de esta funcion es convertir los datos que se
    almacenan en cadenas en datos con valores numericos
    
    """
    def crear_matriz(self, datos: DataFrame):
        num_col = 20
        num_filas = 0
        matriz = []

        # Se calcula el numero de filas
        for fila in datos.iterrows():
            num_filas = num_filas + 1

        for i in range(num_filas):
            matriz.append([0] * num_col)

        # Rellenamos la matriz
        for i in range(num_filas):
            for j in range(num_col):
                info_item = str(datos.iloc[i][j])
                num = 0

                # Sexo
                if j == 1:
                    if info_item == 'Masculino':
                        num = 1
                    elif info_item == 'Femenino':
                        num = 2

                    matriz[i][j] = num

                # ¿Compra regularmente por Internet?
                if j == 2:
                    if info_item == 'Sí':
                        num = 1
                    elif info_item == 'No':
                        num = 2

                    matriz[i][j] = num

                # Si es la columna orden datos del comprador
                if j == 3:
                    if info_item == 'Teléfono,  Apellidos, Nombres, Documento':
                        num = 1
                    elif info_item == 'Nombres, Apellidos, Documento, Teléfono':
                        num = 2
                    elif info_item == 'Documento, Nombres, Apellidos, Teléfono':
                        num = 3
                    elif info_item == 'Nombres, Apellidos, Teléfono, Documento':
                        num = 4

                    matriz[i][j] = num

                # Si es la columna orden datos de envio
                if j == 4:
                    if info_item == 'Ciudad, Departamento, Domicilio, Teléfono, Nombre de quien recibe':
                        num = 1
                    elif info_item == 'Nombre de quien recibe, Domicilio, Ciudad, Departamento, Teléfono':
                        num = 2
                    elif info_item == 'Departamento, Ciudad, Domicilio, Nombre de quien recibe, Teléfono':
                        num = 3
                    elif info_item == 'Nombre de quien recibe, Teléfono, Departamento, Ciudad, Domicilio':
                        num = 4

                    matriz[i][j] = num

                # Si es la columna orden datos de producto
                if j == 5:
                    if info_item == 'Precio - Nombre - Costo envió - Imagen - Cantidad Disponible':
                            num = 1
                    elif info_item == 'Nombre - Precio - Imagen - Costo envió - Cantidad Disponible':
                            num = 2
                    elif info_item == 'Imagen - Nombre - Precio - Cantidad Disponible - Costo envió':
                            num = 3
                    elif info_item == 'Nombre - Imagen - Precio - Cantidad Disponible - Costo envió':
                            num = 4
                    elif info_item == 'Cantidad Disponible - Nombre - Imagen - Precio - Costo envió':
                            num = 5

                    matriz[i][j] = num

                # ¿Cual de las siguientes partes deberia encabezar la seccion de envio?
                if j == 6:
                    if info_item == 'Forma de entrega':
                            num = 1
                    elif info_item == 'Dirección del domicilio':
                            num = 2
                    elif info_item == 'Fecha de entrega':
                            num = 3
                    elif info_item == 'Telefono':
                            num = 4
                    elif info_item == 'Nombre de quien recibe':
                            num = 5

                    matriz[i][j] = num

                # ¿Cual de los siguientes datos considera el mas importante en el formato de compra?
                if j == 7:
                    if info_item == 'Nombre del producto':
                            num = 1
                    elif info_item == 'Precio del producto':
                            num = 2
                    elif info_item == 'Imagen del producto':
                            num = 3
                    elif info_item == 'Nombre del comprador':
                            num = 4
                    elif info_item == 'Domicilio de envio':
                            num = 5

                    matriz[i][j] = num

                # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del comprador?
                if j == 8:
                    if info_item == 'Nombre':
                            num = 1
                    elif info_item == 'Apellidos':
                            num = 2
                    elif info_item == 'Documento':
                            num = 3
                    elif info_item == 'Teléfono':
                            num = 4
                    elif info_item == 'Correo electronico':
                            num = 5

                    matriz[i][j] = num

                # ¿Cual de los siguientes datos consideraria mas importante para el diseño de la seccion del producto?
                if j == 9:
                    if info_item == 'Nombre del producto':
                            num = 1
                    elif info_item == 'Precio del producto':
                            num = 2
                    elif info_item == 'Cantidad disponible':
                            num = 3
                    elif info_item == 'Imagen del producto':
                            num = 4

                    matriz[i][j] = num

                # ¿Cuales datos del comprador consideran que son obligatorios?*
                if j == 10:
                    if info_item == 'Nombre del producto':
                            num = 1

                    matriz[i][j] = num

                    #¿Cuales datos del envio considera que son obligatorios?
                if (j == 11):
                    if (info_item == 'Nombre del producto'):
                            num = 1

                    matriz[i][j] = num

                #¿Cuales datos del producto considera que son obligatorios?
                if (j == 12):
                    if (info_item == 'Teléfono,  Apellidos, Nombres, Documento'):
                        num = 1

                    matriz[i][j] = num

                #¿Cuantos años tienes?
                if(j == 13):
                    matriz[i][j] = info_item

                #¿Donde cree que deberia ir la informacion del comprador, del envio y del producto?
                if (j == 14 or j == 15 or j == 16):
                    if (info_item == 'Izquierda'):
                        num = 1
                    elif (info_item == 'Derecha'):
                        num = 2
                    elif (info_item == 'Arriba'):
                        num = 3
                    elif (info_item == 'Centro'):
                        num = 4
                    elif (info_item == 'Abajo'):
                        num = 5

                    matriz[i][j] = num

                #¿Es usted una persona asalariada?
                if(j == 17):
                    if(info_item == 'Sí'):
                        num = 1
                    elif(info_item == 'No'):
                        num = 2

                    matriz[i][j] = num

                #¿Nombre y apellido deberian estan juntos o por separado?
                if(j == 18):
                    if(info_item == 'Juntos'):
                        num = 1
                    elif(info_item == 'Separados'):
                        num = 2

                    matriz[i][j] = num

                #¿Son claras las preguntas?
                if(j == 19):
                    matriz[i][j] = info_item


    # Permite volver a la ventana principal
    def action_button_volver(self):
        self.close()

