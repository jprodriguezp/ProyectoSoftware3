# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'PrincipalScaleInvoice.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QDir

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(725, 380)
        MainWindow.setMinimumSize(QtCore.QSize(725, 380))
        MainWindow.setMaximumSize(QtCore.QSize(725, 380))
        icon = QtGui.QIcon()
        ruta = 'icons/ic_escritorio.png'
        icon.addPixmap(QtGui.QPixmap(QDir.toNativeSeparators(ruta)), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("#btnVerRespuestas:hover{\n"
"background-color: rgb(65, 195, 0);\n"
"}\n"
"#btnPredicciones:hover{\n"
"background-color: rgb(65, 195, 0);\n"
"}\n"
"#btnFormulario:hover{\n"
"background-color: rgb(65, 195, 0);\n"
"}")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(0, 80, 721, 61))
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(28)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.gridLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(30, 170, 661, 151))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.btnVerRespuestas = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnVerRespuestas.sizePolicy().hasHeightForWidth())
        self.btnVerRespuestas.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(20)
        self.btnVerRespuestas.setFont(font)
        self.btnVerRespuestas.setObjectName("btnVerRespuestas")
        self.gridLayout.addWidget(self.btnVerRespuestas, 0, 0, 1, 1)
        self.btnPredicciones = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnPredicciones.sizePolicy().hasHeightForWidth())
        self.btnPredicciones.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(22)
        self.btnPredicciones.setFont(font)
        self.btnPredicciones.setObjectName("btnPredicciones")
        self.gridLayout.addWidget(self.btnPredicciones, 0, 1, 1, 1)
        self.btnFormulario = QtWidgets.QPushButton(self.gridLayoutWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.btnFormulario.sizePolicy().hasHeightForWidth())
        self.btnFormulario.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(22)
        self.btnFormulario.setFont(font)
        self.btnFormulario.setObjectName("btnFormulario")
        self.gridLayout.addWidget(self.btnFormulario, 0, 2, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "ScaleInvoice"))
        self.label.setText(_translate("MainWindow", "ScaleInvoice"))
        self.btnVerRespuestas.setText(_translate("MainWindow", "Ver Repuestas"))
        self.btnPredicciones.setText(_translate("MainWindow", "Predicciones"))
        self.btnFormulario.setText(_translate("MainWindow", "Formulario"))

