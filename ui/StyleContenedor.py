# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'StyleContenedor.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.Contenedor = QtWidgets.QFrame(self.centralwidget)
        self.Contenedor.setGeometry(QtCore.QRect(10, 80, 311, 311))
        self.Contenedor.setStyleSheet("border: 0.5px solid gray;\n"
"border-radius: 15px;\n"
"background:white;")
        self.Contenedor.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.Contenedor.setFrameShadow(QtWidgets.QFrame.Raised)
        self.Contenedor.setObjectName("Contenedor")
        self.LineaTitle = QtWidgets.QFrame(self.Contenedor)
        self.LineaTitle.setGeometry(QtCore.QRect(0, 26, 311, 16))
        self.LineaTitle.setFrameShape(QtWidgets.QFrame.HLine)
        self.LineaTitle.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.LineaTitle.setObjectName("LineaTitle")
        self.label = QtWidgets.QLabel(self.Contenedor)
        self.label.setGeometry(QtCore.QRect(130, 3, 41, 20))
        self.label.setStyleSheet("font: 75 12pt \"MS Shell Dlg 2\";")
        self.label.setObjectName("label")
        self.line = QtWidgets.QFrame(self.Contenedor)
        self.line.setGeometry(QtCore.QRect(0, 250, 311, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.pushButton = QtWidgets.QPushButton(self.Contenedor)
        self.pushButton.setGeometry(QtCore.QRect(10, 280, 75, 23))
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Title"))
        self.pushButton.setText(_translate("MainWindow", "Enviar"))

