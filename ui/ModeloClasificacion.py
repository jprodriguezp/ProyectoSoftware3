from __future__ import print_function
from PyQt5.QtCore import QDir
from oauth2client.service_account import ServiceAccountCredentials
import gspread
import pandas as pd


"""Funcion que permite importar el conjunto de datos.
La funcion importa el conjunto de datos almacenado en 
Google Drive

"""
def leer_data_set():
    # use creds to create a client to interact with the Google Drive API
    SCOPE = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
    ruta = '..\ScaleInvoice-1c6ae0bd78ac.json'
    creds = ServiceAccountCredentials.from_json_keyfile_name(QDir.toNativeSeparators(ruta), SCOPE)
    client = gspread.authorize(creds)
    # Find a workbook by name and open the firts sheet
    # Make sure you use the right name here5

    sheet = client.open("Compra por Internet (respuestas)").sheet1

    print(type(sheet))

    results = pd.DataFrame(sheet.get_all_records())

    #for cell in all_cells:
        #print(cell.value)

    return results

