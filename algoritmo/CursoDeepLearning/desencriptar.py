def obtener_respuesta(numero):
    if numero == 1:
        var = "Arriba"
        return var
    if numero == 2:
        var = "Derecha"
        return var
    if numero == 3:
        var = "Centro"
        return var
    if numero == 4:
        var = "Abajo"
        return var
    if numero == 5:
        var = "Izquierda"
        return var
    if numero == 6:
        var = "Documento, Nombres, Apellidos, Teléfono"
        return var
    if numero == 7:
        var = "Nombres, Apellidos, Documento, Teléfono"
        return var
    if numero == 8:
        var = "Nombres, Apellidos, Teléfono, Documento"
        return var
    if numero == 9:
        var = "Teléfono,  Apellidos, Nombres, Documento"
        return var
    if numero == 10:
        var = "Documento de identidad"
        return var
    if numero == 11:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Correo electrónico"
        return var
    if numero == 12:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador"
        return var
    if numero == 13:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Correo electrónico"
        return var
    if numero == 14:
        var = "Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad"
        return var
    if numero == 15:
        var = "Nombres y Apellidos, Teléfono del comprador"
        return var
    if numero == 16:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Barrio, Correo electrónico"
        return var
    if numero == 17:
        var = "Nombres y Apellidos"
        return var
    if numero == 18:
        var = "Documento de identidad, Nombres y Apellidos"
        return var
    if numero == 19:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Barrio, Correo electrónico"
        return var
    if numero == 20:
        var = "Nombre"
        return var
    if numero == 21:
        var = "Correo electronico"
        return var
    if numero == 22:
        var = "Documento"
        return var
    if numero == 23:
        var = "Apellidos"
        return var
    if numero == 24:
        var = "Separados"
        return var
    if numero == 25:
        var = "Juntos"
        return var
    if numero == 26:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible, Descripción, Costo de envío"
        return var
    if numero == 27:
        var = "Nombre del producto, Precio del producto, Cantidad disponible, Costo de envío"
        return var
    if numero == 28:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Costo de envío"
        return var
    if numero == 29:
        var = "Precio del producto, Imagen del producto, Descripción"
        return var
    if numero == 30:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Descripción"
        return var
    if numero == 31:
        var = "Nombre del producto, Precio del producto, Cantidad disponible, Descripción, Costo de envío"
        return var
    if numero == 32:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible"
        return var
    if numero == 33:
        var = "Nombre del producto"
        return var
    if numero == 34:
        var = "Nombre del producto, Precio del producto, Costo de envío"
        return var
    if numero == 35:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Descripción, Costo de envío"
        return var
    if numero == 36:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Descripción, Costo de envío, Codigo del producto"
        return var
    if numero == 37:
        var = "Descripción"
        return var
    if numero == 38:
        var = "Nombre del producto, Precio del producto, Descripción"
        return var
    if numero == 39:
        var = "Imagen del producto"
        return var
    if numero == 40:
        var = "Precio del producto"
        return var
    if numero == 41:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible, Costo de envío"
        return var
    if numero == 42:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible, Descripción, Costo de envío, Garantia"
        return var
    if numero == 43:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible, Descripción"
        return var
    if numero == 44:
        var = "Nombre del producto, Imagen del producto"
        return var
    if numero == 45:
        var = "Nombre del producto, Precio del producto, Cantidad disponible, Descripción, Costo de envío, Discriminación iva"
        return var
    if numero == 46:
        var = "Precio del producto, Imagen del producto, Cantidad disponible, Costo de envío"
        return var
    if numero == 47:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible, Descripción, Costo de envío, Que sean dos :D"
        return var
    if numero == 48:
        var = "Nombre del producto, Precio del producto, Imagen del producto, Cantidad disponible, Descripción, Costo de envío, Reseñas sobre el producto"
        return var
    if numero == 49:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad"
        return var
    if numero == 50:
        var = "Documento de identidad, Departamento y Ciudad, Barrio, Correo electrónico"
        return var
    if numero == 51:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Correo electrónico, Flaco, regáleme una gomita pues :v"
        return var
    if numero == 52:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Barrio, Correo electrónico, Código Postal"
        return var
    if numero == 53:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Barrio"
        return var
    if numero == 54:
        var = "Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Barrio"
        return var
    if numero == 55:
        var = "Nombres y Apellidos, Teléfono del comprador, Correo electrónico"
        return var
    if numero == 56:
        var = "Documento de identidad, Nombres y Apellidos, Departamento y Ciudad, Correo electrónico"
        return var
    if numero == 57:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Barrio, Correo electrónico, Dirección de residencia"
        return var
    if numero == 58:
        var = "Documento de identidad, Nombres y Apellidos, Correo electrónico"
        return var
    if numero == 59:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Barrio, Correo electrónico, direccion"
        return var
    if numero == 60:
        var = "Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Correo electrónico"
        return var
    if numero == 61:
        var = "Nombres y Apellidos, Correo electrónico"
        return var
    if numero == 62:
        var = "Documento de identidad, Teléfono del comprador, Correo electrónico"
        return var
    if numero == 63:
        var = "Correo electrónico"
        return var
    if numero == 64:
        var = "Nombres y Apellidos, Teléfono del comprador, Correo electrónico, Dirección"
        return var
    if numero == 65:
        var = "Documento de identidad, Nombres y Apellidos, Teléfono del comprador, Departamento y Ciudad, Correo electrónico, Direccion"
        return var
    if numero == 66:
        var = "Nombre - Precio - Imagen - Costo envió - Cantidad Disponible"
        return var
    if numero == 67:
        var = "Imagen - Nombre - Precio - Cantidad Disponible - Costo envió"
        return var
    if numero == 68:
        var = "Cantidad Disponible - Nombre - Imagen - Precio - Costo envió"
        return var
    if numero == 69:
        var = "Nombre - Imagen - Precio - Cantidad Disponible - Costo envió"
        return var
    if numero == 70:
        var = "Precio - Nombre - Costo envió - Imagen - Cantidad Disponible"
        return var
    if numero == 71:
        var = "Cantidad disponible"
        return var
    if numero == 72:
        var = "Nombre de quien recibe, Domicilio, Ciudad, Departamento, Teléfono"
        return var
    if numero == 73:
        var = "Ciudad, Departamento, Domicilio, Teléfono, Nombre de quien recibe"
        return var
    if numero == 74:
        var = "Nombre de quien recibe, Teléfono, Departamento, Ciudad, Domicilio"
        return var
    if numero == 75:
        var = "Departamento, Ciudad, Domicilio, Nombre de quien recibe, Teléfono"
        return var
    if numero == 76:
        var = "Departamento, Ciudad, Domicilio, Teléfono de contacto"
        return var
    if numero == 77:
        var = "Departamento, Ciudad, Nombre y apellido de quien recibe"
        return var
    if numero == 78:
        var = "Departamento, Ciudad, Domicilio, Nombre y apellido de quien recibe"
        return var
    if numero == 79:
        var = "Ciudad, Domicilio, Teléfono de contacto"
        return var
    if numero == 80:
        var = "Departamento, Ciudad, Domicilio, Nombre y apellido de quien recibe, Teléfono de contacto"
        return var
    if numero == 81:
        var = "Departamento, Ciudad, Nombre y apellido de quien recibe, Teléfono de contacto"
        return var
    if numero == 82:
        var = "Ciudad, Domicilio, Nombre y apellido de quien recibe, Teléfono de contacto"
        return var
    if numero == 83:
        var = "Teléfono de contacto"
        return var
    if numero == 84:
        var = "Domicilio"
        return var
    if numero == 85:
        var = "Departamento, Ciudad, Teléfono de contacto"
        return var
    if numero == 86:
        var = "Departamento"
        return var
    if numero == 87:
        var = "Ciudad, Domicilio, Nombre y apellido de quien recibe"
        return var
    if numero == 88:
        var = "Departamento, Domicilio, Nombre y apellido de quien recibe"
        return var
    if numero == 89:
        var = "Nombre y apellido de quien recibe"
        return var
    if numero == 90:
        var = "Domicilio, Nombre y apellido de quien recibe"
        return var
    if numero == 91:
        var = "Dirección del Domicilio"
        return var
    if numero == 92:
        var = "Fecha de entrega"
        return var
    if numero == 93:
        var = "Nombre de quien recibe"
        return var
    if numero == 94:
        var = "Forma de entrega"
        return var
    if numero == 95:
        var = "Telefono"
        return var
    if numero == 96:
        var = "Nombre del comprador"
        return var
    if numero == 97:
        var = "Domicilio de envio"
        return var
    if numero == 98:
        var = "Sí"
        return var
    if numero == 99:
        var = "No"
        return var
    if numero == 100:
        var = "Masculino"
        return var
    if numero == 101:
        var = "Femenino"
        return var

def definir_numero(texto:str):
    if texto.__eq__("Sí"):
        var = 98
        return var
    if texto.__eq__("No"):
        var = 99
        return var
    if texto.__eq__("Masculino"):
        var = 100
        return var
    if texto.__eq__("Femenino"):
        var = 101
        return var


