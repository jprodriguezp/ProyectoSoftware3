import pandas as pd
from keras.models import Sequential
from keras.layers import *

training_data_df = pd.read_csv("sales_data_training_scaled.csv")

X = training_data_df[['Cuantos anos tienes?', 'Compra regularmente por Internet?'
                      , 'Son claras las preguntas? Responda de acuerdo a una escala del 1 al 5. Siendo 5 muy claras y 1 dificiles de comprender.', 'Sexo'
                      , 'Es usted una persona asalariada?']]
Y = training_data_df[['Donde cree que debera ir la informacin del comprador', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?'
                      , 'Cuales datos del comprador considera que son obligatorios?', 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?'
                      , 'nombre y apellido deberian estar juntos o por separado?', 'Donde cree que deberia ir la informacion del producto?'
                      , 'Cuales datos del producto considera que son obligatorios ?', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?'
                      , 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?', 'Donde cree que deberian ir la informacion del envio?'
                      , 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?', 'Cuales datos del envio considera que son obligatorios?'
                      , 'Cual de las siguientes partes deberia encabezar la seccion de envio?', 'Cual de los siguientes datos considera el mas importante en el formato de compra?']].values

# Define the model
model = Sequential()
model.add(Dense(50, input_dim=5, activation='relu'))
model.add(Dense(100, activation='relu'))
model.add(Dense(50, activation='relu'))
model.add(Dense(14, activation='linear'))
model.compile(loss='mean_squared_error', optimizer='adam')

# Train the model
model.fit(
    X,
    Y,
    epochs=100,
    shuffle=True,
    verbose=2
)

# Load the separate test data set
test_data_df = pd.read_csv("sales_data_test_scaled.csv")

X_test = test_data_df[['Cuantos anos tienes?', 'Compra regularmente por Internet?'
                      , 'Son claras las preguntas? Responda de acuerdo a una escala del 1 al 5. Siendo 5 muy claras y 1 dificiles de comprender.', 'Sexo'
                      , 'Es usted una persona asalariada?']]
Y_test = test_data_df[['Donde cree que debera ir la informacin del comprador', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?'
                      , 'Cuales datos del comprador considera que son obligatorios?', 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?'
                      , 'nombre y apellido deberian estar juntos o por separado?', 'Donde cree que deberia ir la informacion del producto?'
                      , 'Cuales datos del producto considera que son obligatorios ?', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?'
                      , 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?', 'Donde cree que deberian ir la informacion del envio?'
                      , 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?', 'Cuales datos del envio considera que son obligatorios?'
                      , 'Cual de las siguientes partes deberia encabezar la seccion de envio?', 'Cual de los siguientes datos considera el mas importante en el formato de compra?']].values

test_error_rate = model.evaluate(X_test, Y_test, verbose=0)
print("The mean squared error (MSE) for the test data set is: {}".format(test_error_rate))

# Save the model to disk

model.save("trained_model(Q1).h5")
print("Modelo guardado en su disco")
