import pandas as pd
import numpy as np
from keras.models import load_model
from algoritmo.CursoDeepLearning import desencriptar as di

#respuesta = ""

#model = load_model("../../ui/trained_model(Q1).h5")

#X = pd.read_csv("../../ui/propose_forms.csv")[['Cuantos anos tienes?', 'Compra regularmente por Internet?'
#                      , 'Son claras las preguntas? Responda de acuerdo a una escala del 1 al 5. Siendo 5 muy claras y 1 dificiles de comprender.', 'Sexo'
#                      , 'Es usted una persona asalariada?']]
#prediction = model.predict(X)


# Grab just the first element of the first prediction (since we only have one)
#prediction1 = prediction[0][0]
#prediction2 = prediction[0][1]
#prediction3 = prediction[0][2]
#prediction4 = prediction[0][3]
#prediction5 = prediction[0][4]
#prediction6 = prediction[0][5]
#prediction7 = prediction[0][6]
#prediction8 = prediction[0][7]
#prediction9 = prediction[0][8]
#prediction10 = prediction[0][9]
#prediction11 = prediction[0][10]
#prediction12 = prediction[0][11]
#prediction13 = prediction[0][12]
#prediction14 = prediction[0][13]


# Re-scale the data from the 0-to-1 range back to dollars
# These constants are from when the data was originally scaled down to the 0-to-1 range
#prediction = prediction + 0.1159
#prediction = prediction / 0.0000036968


#print("Prediccion :Cual de los siguientes datos considera el mas importante en el formato de compra? {}".format(prediction1))
#print("Prediccion :Cual de las siguientes partes deberia encabezar la seccion de envio? {}".format(prediction2))
#print("Prediccion :Cuales datos del envio considera que son obligatorios? {}".format(prediction3))
#print("Prediccion :Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio? {}".format(prediction4))
#print("Prediccion :Donde cree que deberian ir la informacion del envio? {}".format(prediction5))
#print("Prediccion :Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto? {}".format(prediction6))
#print("Prediccion :Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto? {}".format(prediction7))
#print("Prediccion :Cuales datos del producto considera que son obligatorios ? {}".format(prediction8))
#print("Prediccion :Donde cree que deberia ir la informacion del producto? {}".format(prediction9))
#print("Prediccion :nombre y apellido deberian estar juntos o por separado? {}".format(prediction10))
#print("Prediccion :Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador? {}".format(prediction11))
#print("Prediccion :Cuales datos del comprador considera que son obligatorios? {}".format(prediction12))
#print("Prediccion :Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador? {}".format(prediction13))
#print("Prediccion :Donde cree que debera ir la informacin del comprador {}".format(prediction14))



##------------------------------------------------------------------------------
## Purpose: Funcion que busca encontrar el numero de tipo float generados aleatoriamente
##
## Param:
##
## Author:  Juan Pablo Rodriguez Patino
##          Camilo Angel Hurtado
##
## Return: matriz llena de numeros de tipo float generados aleatoriamente
##Funcion que busca encontrar el numero más cerca a la prediccion
def buscar_numero_mas_cerca(numero, texto):
    dataset = pd.read_csv("scaleInvoice_test.csv")[[texto]]
    centinela = -1
    resta_menor = 2000

    dataasmatriz = dataset.as_matrix()

    for x in dataasmatriz:
        resta:int = numero - x
        resta = resta.__abs__()
        if resta < resta_menor:
            resta_menor = resta
            centinela = x

    return centinela





##Funcion que busca desemcriptar las predicciones en respuestas del dataset
def desemcriptar_respuesta(frame):

    dataset = pd.read_csv("scaleInvoice_test.csv")[['Donde cree que debera ir la informacin del comprador', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?'
                      , 'Cuales datos del comprador considera que son obligatorios?', 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?'
                      , 'nombre y apellido deberian estar juntos o por separado?', 'Donde cree que deberia ir la informacion del producto?'
                      , 'Cuales datos del producto considera que son obligatorios ?', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?'
                      , 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?', 'Donde cree que deberian ir la informacion del envio?'
                      , 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?', 'Cuales datos del envio considera que son obligatorios?'
                      , 'Cual de las siguientes partes deberia encabezar la seccion de envio?', 'Cual de los siguientes datos considera el mas importante en el formato de compra?']]


    arreglo:()=[]
    numero_1 = buscar_numero_mas_cerca(frame[0][13], 'Donde cree que debera ir la informacin del comprador')
    arreglo.append(numero_1)
    numero_2 = buscar_numero_mas_cerca(frame[0][12], 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?')
    arreglo.append(numero_2)
    numero_3 = buscar_numero_mas_cerca(frame[0][11], 'Cuales datos del comprador considera que son obligatorios?')
    arreglo.append(numero_3)
    numero_4 = buscar_numero_mas_cerca(frame[0][10], 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?')
    arreglo.append(numero_4)
    numero_5 = buscar_numero_mas_cerca(frame[0][9], 'nombre y apellido deberian estar juntos o por separado?')
    arreglo.append(numero_5)
    numero_6 = buscar_numero_mas_cerca(frame[0][8], 'Donde cree que deberia ir la informacion del producto?')
    arreglo.append(numero_6)
    numero_7 = buscar_numero_mas_cerca(frame[0][7], 'Cuales datos del producto considera que son obligatorios ?')
    arreglo.append(numero_7)
    numero_8 = buscar_numero_mas_cerca(frame[0][6], 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?')
    arreglo.append(numero_8)
    numero_9 = buscar_numero_mas_cerca(frame[0][5], 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?')
    arreglo.append(numero_9)
    numero_10 = buscar_numero_mas_cerca(frame[0][4], 'Donde cree que deberian ir la informacion del envio?')
    arreglo.append(numero_10)
    numero_11 = buscar_numero_mas_cerca(frame[0][3], 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?')
    arreglo.append(numero_11)
    numero_12 = buscar_numero_mas_cerca(frame[0][2], 'Cuales datos del envio considera que son obligatorios?')
    arreglo.append(numero_12)
    numero_13 = buscar_numero_mas_cerca(frame[0][1], 'Cual de las siguientes partes deberia encabezar la seccion de envio?')
    arreglo.append(numero_13)
    numero_14 = buscar_numero_mas_cerca(frame[0][0], 'Cual de los siguientes datos considera el mas importante en el formato de compra?')
    arreglo.append(numero_14)

    cadenas:()=[]
    cadena_1 = di.obtener_respuesta(numero_1)
    cadena_2 = di.obtener_respuesta(numero_2)
    cadena_3 = di.obtener_respuesta(numero_3)
    cadena_4 = di.obtener_respuesta(numero_4)
    cadena_5 = di.obtener_respuesta(numero_5)
    cadena_6 = di.obtener_respuesta(numero_6)
    cadena_7 = di.obtener_respuesta(numero_7)
    cadena_8 = di.obtener_respuesta(numero_8)
    cadena_9 = di.obtener_respuesta(numero_9)
    cadena_10 = di.obtener_respuesta(numero_10)
    cadena_11 = di.obtener_respuesta(numero_11)
    cadena_12 = di.obtener_respuesta(numero_12)
    cadena_13 = di.obtener_respuesta(numero_13)
    cadena_14 = di.obtener_respuesta(numero_14)
    cadenas.append(cadena_1)
    cadenas.append(cadena_2)
    cadenas.append(cadena_3)
    cadenas.append(cadena_4)
    cadenas.append(cadena_5)
    cadenas.append(cadena_6)
    cadenas.append(cadena_7)
    cadenas.append(cadena_8)
    cadenas.append(cadena_9)
    cadenas.append(cadena_10)
    cadenas.append(cadena_11)
    cadenas.append(cadena_12)
    cadenas.append(cadena_13)
    cadenas.append(cadena_14)
    respuesta = cadenas[0], cadenas[1],cadenas[2],cadenas[3],cadenas[4],cadenas[5],cadenas[6],cadenas[7],cadenas[8],cadenas[9],cadenas[10],cadenas[11],cadenas[12],cadenas[13]
















##Funcion que busca obtener una prediccion.
def obtener_prediccion():
    1
    #data = [[int(round(prediction1)), int(round(prediction2)), int(round(prediction3)), int(round(prediction4)), int(round(prediction5)), int(round(prediction6)),
    #         int(round(prediction7)), int(round(prediction8))
    #            , int(round(prediction9)), int(round(prediction10)), int(round(prediction11)), int(round(prediction12)), int(round(prediction13)), int(round(prediction14))]]
    #frame = pd.DataFrame(data=data)
    #respuesta =  desemcriptar_respuesta(data)





def obtener():
    model = load_model("../../ui/trained_model(Q1).h5")

    X = pd.read_csv("../../ui/propose_forms.csv")[['Cuantos anos tienes?', 'Compra regularmente por Internet?',
                                          'Son claras las preguntas? Responda de acuerdo a una escala del 1 al 5. Siendo 5 muy claras y 1 dificiles de comprender.',
                                          'Sexo', 'Es usted una persona asalariada?']]
    prediction = model.predict(X)

    # Grab just the first element of the first prediction (since we only have one)
    prediction1 = prediction[0][0]
    prediction2 = prediction[0][1]
    prediction3 = prediction[0][2]
    prediction4 = prediction[0][3]
    prediction5 = prediction[0][4]
    prediction6 = prediction[0][5]
    prediction7 = prediction[0][6]
    prediction8 = prediction[0][7]
    prediction9 = prediction[0][8]
    prediction10 = prediction[0][9]
    prediction11 = prediction[0][10]
    prediction12 = prediction[0][11]
    prediction13 = prediction[0][12]
    prediction14 = prediction[0][13]

    data = [[int(round(prediction1)), int(round(prediction2)), int(round(prediction3)), int(round(prediction4)),
             int(round(prediction5)), int(round(prediction6)),
             int(round(prediction7)), int(round(prediction8))
                , int(round(prediction9)), int(round(prediction10)), int(round(prediction11)), int(round(prediction12)),
             int(round(prediction13)), int(round(prediction14))]]
    f = pd.DataFrame(data=data)
    dataset = pd.read_csv("scaleInvoice_test.csv")[['Donde cree que debera ir la informacin del comprador',
                                                    'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?'
        , 'Cuales datos del comprador considera que son obligatorios?',
                                                    'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?'
        , 'nombre y apellido deberian estar juntos o por separado?',
                                                    'Donde cree que deberia ir la informacion del producto?'
        , 'Cuales datos del producto considera que son obligatorios ?',
                                                    'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?'
        , 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?',
                                                    'Donde cree que deberian ir la informacion del envio?'
        , 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?',
                                                    'Cuales datos del envio considera que son obligatorios?'
        , 'Cual de las siguientes partes deberia encabezar la seccion de envio?',
                                                    'Cual de los siguientes datos considera el mas importante en el formato de compra?']]

    arreglo: () = []

    frame = f.as_matrix()
    numero_1 = buscar_numero_mas_cerca(frame[0][13], 'Donde cree que debera ir la informacin del comprador')
    arreglo.append(numero_1)
    numero_2 = buscar_numero_mas_cerca(frame[0][12],
                                       'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?')
    arreglo.append(numero_2)
    numero_3 = buscar_numero_mas_cerca(frame[0][11], 'Cuales datos del comprador considera que son obligatorios?')
    arreglo.append(numero_3)
    numero_4 = buscar_numero_mas_cerca(frame[0][10],
                                       'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?')
    arreglo.append(numero_4)
    numero_5 = buscar_numero_mas_cerca(frame[0][9], 'nombre y apellido deberian estar juntos o por separado?')
    arreglo.append(numero_5)
    numero_6 = buscar_numero_mas_cerca(frame[0][8], 'Donde cree que deberia ir la informacion del producto?')
    arreglo.append(numero_6)
    numero_7 = buscar_numero_mas_cerca(frame[0][7], 'Cuales datos del producto considera que son obligatorios ?')
    arreglo.append(numero_7)
    numero_8 = buscar_numero_mas_cerca(frame[0][6],
                                      'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?')
    arreglo.append(numero_8)
    numero_9 = buscar_numero_mas_cerca(frame[0][5],
                                       'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?')
    arreglo.append(numero_9)
    numero_10 = buscar_numero_mas_cerca(frame[0][4], 'Donde cree que deberian ir la informacion del envio?')
    arreglo.append(numero_10)
    numero_11 = buscar_numero_mas_cerca(frame[0][3],
                                        'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?')
    arreglo.append(numero_11)
    numero_12 = buscar_numero_mas_cerca(frame[0][2], 'Cuales datos del envio considera que son obligatorios?')
    arreglo.append(numero_12)
    numero_13 = buscar_numero_mas_cerca(frame[0][1],
                                        'Cual de las siguientes partes deberia encabezar la seccion de envio?')
    arreglo.append(numero_13)
    numero_14 = buscar_numero_mas_cerca(frame[0][0],
                                        'Cual de los siguientes datos considera el mas importante en el formato de compra?')
    arreglo.append(numero_14)

    cadenas: () = []
    cadena_1 = di.obtener_respuesta(numero_1)
    cadena_2 = di.obtener_respuesta(numero_2)
    cadena_3 = di.obtener_respuesta(numero_3)
    cadena_4 = di.obtener_respuesta(numero_4)
    cadena_5 = di.obtener_respuesta(numero_5)
    cadena_6 = di.obtener_respuesta(numero_6)
    cadena_7 = di.obtener_respuesta(numero_7)
    cadena_8 = di.obtener_respuesta(numero_8)
    cadena_9 = di.obtener_respuesta(numero_9)
    cadena_10 = di.obtener_respuesta(numero_10)
    cadena_11 = di.obtener_respuesta(numero_11)
    cadena_12 = di.obtener_respuesta(numero_12)
    cadena_13 = di.obtener_respuesta(numero_13)
    cadena_14 = di.obtener_respuesta(numero_14)
    cadenas.append(cadena_1)
    cadenas.append(cadena_2)
    cadenas.append(cadena_3)
    cadenas.append(cadena_4)
    cadenas.append(cadena_5)
    cadenas.append(cadena_6)
    cadenas.append(cadena_7)
    cadenas.append(cadena_8)
    cadenas.append(cadena_9)
    cadenas.append(cadena_10)
    cadenas.append(cadena_11)
    cadenas.append(cadena_12)
    cadenas.append(cadena_13)
    cadenas.append(cadena_14)
    respuesta = cadenas[0], cadenas[1], cadenas[2], cadenas[3], cadenas[4], cadenas[5], cadenas[6], cadenas[7], cadenas[
        8], cadenas[9], cadenas[10], cadenas[11], cadenas[12], cadenas[13]
    print(respuesta)
    return respuesta










