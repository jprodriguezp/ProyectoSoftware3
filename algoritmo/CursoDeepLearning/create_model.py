import pandas as pd
from keras.models import Sequential
from keras.layers import *
from PyQt5.QtCore import QDir

training_data_df = pd.read_csv("sales_data_training_scaled.csv")

X = training_data_df.values
Y = training_data_df[['Sexo']].values

# Define the model
model = Sequential()
model.add(Dense(50, input_dim=4, activation='relu'))
model.add(Dense(100, activation='relu'))
model.add(Dense(50, activation='relu'))
model.add(Dense(1, activation='linear'))
model.compile(loss="mean_squared_error", optimizer="adam")
