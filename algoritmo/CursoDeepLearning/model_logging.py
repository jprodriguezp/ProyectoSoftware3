import pandas as pd
import keras
from keras.models import Sequential
from keras.layers import *
from PyQt5.QtCore import QDir

training_data_df = pd.read_csv(QDir.toNativeSeparators("sales_data_training_scaled.csv"))

X = training_data_df[['Cuantos anos tienes?', 'Compra regularmente por Internet?'
                      , 'Son claras las preguntas? Responda de acuerdo a una escala del 1 al 5. Siendo 5 muy claras y 1 dificiles de comprender.', 'Sexo'
                      , 'Es usted una persona asalariada?']]
Y = training_data_df[['Donde cree que debera ir la informacin del comprador', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?'
                      , 'Cuales datos del comprador considera que son obligatorios?', 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?'
                      , 'nombre y apellido deberian estar juntos o por separado?', 'Donde cree que deberia ir la informacion del producto?'
                      , 'Cuales datos del producto considera que son obligatorios ?', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?'
                      , 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?', 'Donde cree que deberian ir la informacion del envio?'
                      , 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?', 'Cuales datos del envio considera que son obligatorios?'
                      , 'Cual de las siguientes partes deberia encabezar la seccion de envio?', 'Cual de los siguientes datos considera el mas importante en el formato de compra?']].values

# Define the model
model = Sequential()
model.add(Dense(50, input_dim=5, activation='relu', name='layer_1'))
model.add(Dense(100, activation='relu', name='layer_2'))
model.add(Dense(50, activation='relu', name='layer_3'))
model.add(Dense(14, activation='linear', name='output_layer'))
model.compile(loss='mean_squared_error', optimizer='adam')

# Create a TensorBoard logger

logger = keras.callbacks.TensorBoard(log_dir="logs2",  write_graph=True, histogram_freq=0)


# Train the model
model.fit(
    X,
    Y,
    epochs=50,
    shuffle=True,
    verbose=2,
    callbacks=[logger]
)


# Load the separate test data set
test_data_df = pd.read_csv("sales_data_test_scaled.csv")

X_test = test_data_df[['Cuantos anos tienes?', 'Compra regularmente por Internet?'
                      , 'Son claras las preguntas? Responda de acuerdo a una escala del 1 al 5. Siendo 5 muy claras y 1 dificiles de comprender.', 'Sexo'
                      , 'Es usted una persona asalariada?']]
Y_test = test_data_df[['Donde cree que debera ir la informacin del comprador', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del comprador?'
                      , 'Cuales datos del comprador considera que son obligatorios?', 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del comprador?'
                      , 'nombre y apellido deberian estar juntos o por separado?', 'Donde cree que deberia ir la informacion del producto?'
                      , 'Cuales datos del producto considera que son obligatorios ?', 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del producto?'
                      , 'Cual de los siguientes datos consideraria mas importante para el diseno de la seccion del producto?', 'Donde cree que deberian ir la informacion del envio?'
                      , 'Cual cree usted que es el orden correcto en el que se deben mostrar los siguientes datos del envio?', 'Cuales datos del envio considera que son obligatorios?'
                      , 'Cual de las siguientes partes deberia encabezar la seccion de envio?', 'Cual de los siguientes datos considera el mas importante en el formato de compra?']].values

test_error_rate = model.evaluate(X_test, Y_test, verbose=2)
print("The mean squared error (MSE) for the test data set is: {}".format(test_error_rate))
