# Crea tu primer MLP en Keras
from random import seed

from keras.models import Sequential
from keras.layers import Dense
import numpy
import pandas as pd
from PyQt5.QtCore import QDir
from keras .preprocessing.text import Tokenizer
from keras.preprocessing.text import text_to_word_sequence

# Fija las semillas aleatorias para la reproducibilidad
numpy.random.seed(7)
# carga los datos
dataset1 = pd.read_csv(QDir.toNativeSeparators("CursoDeepLearning/scaleInvoice_training.csv"))



#print(dataset1)
#dataset2 = numpy.array(md.leer_data_set())


#def clearDataSet(data):
#    results = text_to_word_sequence(data.to_string(columns=None))
#    #print(data.to_string(columns=None))
#    return results


dataset =dataset1.values

#t = Tokenizer()

#t.fit_on_texts(dataset1)

# integer encode documents
#encoded_docs = t.texts_to_matrix(dataset1, mode='count')
#print(encoded_docs)
# dividido en variables de entrada (X) y salida (Y)
X = dataset[:,16:20]
Y = dataset[:,1:15]

# Summarize transformed data
#numpy.set_printoptions(precision=3)

# crea el modelo

model = Sequential()
model.add(Dense(12, input_dim=4, activation='relu'))
model.add(Dense(19, activation='relu'))
model.add(Dense(14, activation='sigmoid'))

# Compila el modelo
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
# Ajusta el modelo
model.fit(X, Y, epochs=150, batch_size=10)


# evaluar el modelo

scores = model.evaluate(X, Y)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))


#model = KerasClassifier(build_fn = model, nb_epoch = 150, batch_size = 10)

#Evaluar el model usando 10-fold através de validaciones con scikit-learn
#kfold = StratifiedKFold(n_folds=10, shuffle = True, random_state = 0)
#results = cross_val_score(model, X, Y, cv = kfold)
# calcula las predicciones
#predictions = model.predict(X)
# redondeamos las predicciones
#rounded = [round(x[0]) for x in predictions]
#print(rounded)


